from django.test import TestCase, SimpleTestCase
from django.utils import timezone
from deep_translator import GoogleTranslator
from base.models import User, Game, Task, Round, Card, UserVote


class UserModelTest(TestCase):
    def test_user_str_method(self):
        self.game = Game.get_instance(mode=1, base_task="testtask")
        user = User.objects.create(game=self.game, username="testuser")
        self.assertEqual(user.username, "testuser")


class GameModelTest(TestCase):
    def setUp(self):
        self.game = Game.get_instance(mode=1, base_task="testtask")

    def test_game_str_method(self):
        self.assertEqual(str(self.game), "Strict")
        self.assertEqual(self.game.base_task, "testtask")
        self.game.get_instance(mode=2, base_task="testtask2")
        self.assertEqual(str(self.game), "Average")
        self.assertEqual(self.game.base_task, "testtask2")

    def test_game_in_factory(self):
        self.assertEqual(self.game, Game.objects.first())

    def test_game_get_instance(self):
        self.assertEqual(self.game, Game.get_instance(mode=1, base_task="testtask"))


class TaskModelTest(TestCase):
    def setUp(self):
        self.game = Game.get_instance(mode=1, base_task="testtask")
        self.task_1 = Task.objects.create(game=self.game, name="testtask")
        self.task_2 = Task.objects.create(game=self.game, name="testtask2")
        self.task_3 = Task.objects.create(game=self.game, name="testtask3")
        self.task_4 = Task.objects.create(game=self.game, name="testtask4")

    def test_task_str_method(self):
        self.assertEqual(str(self.task_1.name), "testtask")
        self.assertEqual(str(self.task_2.name), "testtask2")
        self.assertEqual(str(self.task_3.name), "testtask3")
        self.assertEqual(str(self.task_4.name), "testtask4")

    def test_task_has_game(self):
        self.assertEqual(self.task_1.game, self.game)
        self.assertEqual(self.task_2.game, self.game)
        self.assertEqual(self.task_3.game, self.game)
        self.assertEqual(self.task_4.game, self.game)

    def test_task_not_empty(self):
        task = Task(self.game, name="")
        with self.assertRaises(Exception):
            task.save()

    def test_task_not_too_long(self):
        task = Task(self.game, name="a" * 3000)
        with self.assertRaises(Exception):
            task.save()

    def test_task_unique(self):
        task = Task.objects.create(game=self.game, name="testing_task_unique")
        with self.assertRaises(Exception):
            task = Task.objects.create(game=self.game, name="testing_task_unique")

    def test_task_not_unique(self):
        task = Task.objects.create(game=self.game, name="testing_task_unique")
        task2 = Task.objects.create(game=self.game, name="testing_task_unique2")
        self.assertEqual(task.name, "testing_task_unique")
        self.assertEqual(task2.name, "testing_task_unique2")

    def test_game_has_task(self):
        self.assertEqual(self.game.task_set.count(), 4)


# class UserVote(models.Model):
#     user = models.ForeignKey(User, on_delete=models.CASCADE)
#     task = models.ForeignKey(Task, on_delete=models.CASCADE)
#     vote = models.IntegerField(default=0)
#     voted = models.BooleanField(default=False)
#     round = models.ForeignKey(Round, on_delete=models.CASCADE)


class RoundModelTest(TestCase):
    def setUp(self):
        self.game = Game.get_instance(mode=1, base_task="testtask")
        self.task = Task.objects.create(game=self.game, name="testtask")
        self.round = Round.objects.create(game=self.game, task=self.task, finished=False, final_vote=0)
        self.user = User.objects.create(game=self.game, username="testuser")
        self.userVote = UserVote.objects.create(user=self.user, task=self.task, round=self.round, vote=1, voted=True)


    def test_round_str_method(self):
        self.assertEqual(str(self.round), f"{self.task.name} - round")

    def test_round_has_game(self):
        self.assertEqual(self.round.game, self.game)

    def test_round_has_task(self):
        self.assertEqual(self.round.task, self.task)

    def test_round_has_votes(self):
        self.assertEqual(self.round.votes.count(), 0)

    def test_round_not_finished(self):
        self.assertEqual(self.round.finished, False)

    def test_round_final_vote(self):
        self.assertEqual(self.round.final_vote, 0)

    def test_round_can_be_finished(self):
        self.round.finished = True
        self.round.save()
        self.assertEqual(self.round.finished, True)

    def test_round_can_be_unfinished(self):
        self.round.finished = False
        self.round.save()
        self.assertEqual(self.round.finished, False)

    def test_round_can_be_voted(self):
        self.round.votes.add(self.userVote)
        self.assertEqual(self.round.votes.count(), 1)

    def test_round_can_be_unvoted(self):
        self.round.votes.remove(self.userVote)
        self.assertEqual(self.round.votes.count(), 0)

    def test_round_can_be_voted(self):
        self.round.votes.add(self.userVote)
        self.assertEqual(self.round.votes.count(), 1)


class CardModelTest(TestCase):
    # no need to test this model because it is not used and it is empty
    pass

class UserVoteModelTest(TestCase):
    def setUp(self):
        self.game = Game.get_instance(mode=1, base_task="testtask")
        self.task = Task.objects.create(game=self.game, name="testtask")
        self.round = Round.objects.create(game=self.game, task=self.task, finished=False, final_vote=0)
        self.user = User.objects.create(game=self.game, username="testuser")
        self.userVote = UserVote.objects.create(user=self.user, task=self.task, round=self.round, vote=1, voted=True)

    def test_userVote_str_method(self):
        self.assertEqual(str(self.userVote), f"{self.user.username} - {self.round}")

    def test_userVote_has_user(self):
        self.assertEqual(self.userVote.user, self.user)

    def test_userVote_has_task(self):
        self.assertEqual(self.userVote.task, self.task)

    def test_userVote_has_round(self):
        self.assertEqual(self.userVote.round, self.round)

    def test_userVote_has_vote(self):
        self.assertEqual(self.userVote.vote, 1)

    def test_userVote_has_voted(self):
        self.assertEqual(self.userVote.voted, True)

    def test_userVote_can_be_voted(self):
        self.userVote.voted = True
        self.userVote.save()
        self.assertEqual(self.userVote.voted, True)

    def test_userVote_can_be_unvoted(self):
        self.userVote.voted = False
        self.userVote.save()
        self.assertEqual(self.userVote.voted, False)

    def test_userVote_belongs_to_round(self):
        self.assertEqual(self.userVote.round, self.round)
