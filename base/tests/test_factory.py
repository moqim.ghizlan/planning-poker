from base.factory import ConcreteGameFactory
from base.models import Game, Task, Round, User, UserVote
from django.test import TestCase


class ConcreteGameFactoryTest(TestCase):
    def setUp(self):
        self.game = Game.get_instance(mode=1, base_task="testtask")
        self.task_1 = Task.objects.create(game=self.game, name="testtask")
        self.task_2 = Task.objects.create(game=self.game, name="testtask2")
        self.user_1 = User.objects.create(game=self.game, username="testuser")
        self.user_2 = User.objects.create(game=self.game, username="testuser2")
        self.round = Round.objects.create(game=self.game, task=self.task_1)
        self.userVote_1 = UserVote.objects.create(
            user=self.user_1, task=self.task_1, vote=1, round=self.round
        )
        self.userVote_2 = UserVote.objects.create(
            user=self.user_2, task=self.task_1, vote=1, round=self.round
        )

        self.factory = ConcreteGameFactory.update_current_game()

        self.game.add_task(self.task_1)
        self.game.add_task(self.task_2)
        self.game.add_user(self.user_1)
        self.game.add_user(self.user_2)
        self.game.add_round(self.round)
        self.round.add_vote(self.userVote_1)

    def test_factory_str_method(self):
        self.assertEqual(str(self.factory), "Strict")

    def test_factory_has_game(self):
        self.assertEqual(self.factory, self.game)

    def test_factory_has_tasks(self):
        self.assertEqual(self.factory.tasks.count(), 2)

    def test_factory_has_users(self):
        self.assertEqual(self.factory.users.count(), 2)

    def test_factory_has_rounds(self):
        self.assertEqual(self.factory.rounds.count(), 1)

    def test_factory_has_votes(self):
        self.assertEqual(self.factory.rounds.first().votes.count(), 1)

    def test_factory_has_base_task(self):
        self.assertEqual(self.factory.base_task, "testtask")

    def test_factory_has_start_at(self):
        self.assertNotEqual(self.factory.start_at, None)

    def test_factory_has_not_end_at(self):
        self.assertEqual(self.factory.end_at, None)

    def test_factory_can_by_finished(self):
        self.game.end_game()
        self.factory = ConcreteGameFactory.update_current_game()
        self.assertNotEqual(self.factory.end_at, None)
        self.assertEqual(self.factory.end_at, self.game.end_at)

    def test_factory_can_be_unfinished(
        self,
    ):  # reset the game to avoid errors in other tests
        self.game.end_at = None
        self.game.save()
        self.factory = ConcreteGameFactory.update_current_game()
        self.assertEqual(self.factory.end_at, None)

    def test_factory_has_mode(self):
        self.assertEqual(self.factory.mode, 1)
