from django.test import SimpleTestCase
from django.urls import reverse, resolve
from base.views import (
    index,
    init,
    start_game,
    add_tasks,
    import_tasks,
    add_users,
    reset_game,
    game_end,
    pause_game,
    games_in_pause,
    restart_game_in_pause,
    delete_game_in_pause,
    init_rounds,
    round_results,
    new_vote,
)


class TestUrls(SimpleTestCase):
    def test_index_url_is_resolved(self):
        url = reverse("index")
        self.assertEquals(resolve(url).func, index)

    def test_init_url_is_resolved(self):
        url = reverse("init")
        self.assertEquals(resolve(url).func, init)

    def test_start_game_url_is_resolved(self):
        url = reverse("start_game")
        self.assertEquals(resolve(url).func, start_game)

    def test_add_tasks_url_is_resolved(self):
        url = reverse("add_tasks")
        self.assertEquals(resolve(url).func, add_tasks)

    def test_import_tasks_url_is_resolved(self):
        url = reverse("import_tasks")
        self.assertEquals(resolve(url).func, import_tasks)

    def test_add_users_url_is_resolved(self):
        url = reverse("add_users")
        self.assertEquals(resolve(url).func, add_users)

    def test_reset_game_url_is_resolved(self):
        url = reverse("reset_game")
        self.assertEquals(resolve(url).func, reset_game)

    def test_game_end_url_is_resolved(self):
        url = reverse("game_end")
        self.assertEquals(resolve(url).func, game_end)

    def test_pause_game_url_is_resolved(self):
        url = reverse("pause_game")
        self.assertEquals(resolve(url).func, pause_game)

    def test_games_in_pause_url_is_resolved(self):
        url = reverse("games_in_pause")
        self.assertEquals(resolve(url).func, games_in_pause)

    def test_restart_game_in_pause_url_is_resolved(self):
        url = reverse("restart_game_in_pause")
        self.assertEquals(resolve(url).func, restart_game_in_pause)

    def test_delete_game_in_pause_url_is_resolved(self):
        url = reverse("delete_game_in_pause")
        self.assertEquals(resolve(url).func, delete_game_in_pause)

    def test_init_rounds_url_is_resolved(self):
        url = reverse("init_rounds")
        self.assertEquals(resolve(url).func, init_rounds)

    def test_round_results_url_is_resolved(self):
        url = reverse("round_results", args=[1])
        self.assertEquals(resolve(url).func, round_results)

    def test_new_vote_url_is_resolved(self):
        url = reverse("new_vote", args=[1, 1])
        self.assertEquals(resolve(url).func, new_vote)
