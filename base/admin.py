from django.contrib import admin
from .models import User, Game, Task, UserVote, Round


# on enregistre le modèle User auprès de l'admin de Django pour qu'il soit accessible depuis l'interface d'administration ou cas où on aurait besoin de le modifier
admin.site.register(User)
admin.site.register(Game)
admin.site.register(Task)
admin.site.register(UserVote)
admin.site.register(Round)
