from django.urls import path
from . import views

urlpatterns = [
    path("", views.index, name="index"), # la page d'accueil
    path("init", views.init, name="init"), # la page d'initialisation du jeu
    path("game/start", views.start_game, name="start_game"), # la page de démarrage du jeu
    path("game/add/tasks", views.add_tasks, name="add_tasks"), # la page d'ajout de tâches
    path("game/import/tasks", views.import_tasks, name="import_tasks"), # la page d'import de tâches
    path("game/add/users", views.add_users, name="add_users"), # la page d'ajout d'utilisateurs
    path("game/reset", views.reset_game, name="reset_game"), # la page de reset du jeu
    path("game/end", views.game_end, name="game_end"), # la page de fin de jeu
    path("game/pause", views.pause_game, name="pause_game"), # la page de pause du jeu
    path("game/pause/list", views.games_in_pause, name="games_in_pause"), # la page de liste des jeux en pause
    path(
        "game/pause/restart", views.restart_game_in_pause, name="restart_game_in_pause"
    ), # la page de redémarrage d'un jeu en pause
    path("game/pause/delete", views.delete_game_in_pause, name="delete_game_in_pause"), # la page de suppression d'un jeu en pause
    path("rounds/init", views.init_rounds, name="init_rounds"), # la page d'initialisation des rounds
    path("round/<int:id>/finished", views.round_results, name="round_results"), # la page de résultats d'un round
    path("vote/<int:round_id>/<int:voter_id>", views.new_vote, name="new_vote"),  # la page de vote
]
