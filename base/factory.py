from abc import ABC, abstractmethod
from .models import Game, Task, Round, UserVote, User


class GameFactory(ABC):
    """
    Classe abstraite représentant une fabrique de jeu.

    Cette classe définit des méthodes abstraites pour créer des éléments liés au jeu.

    Methods
    --------
    create_task(game, name):
        Méthode abstraite pour créer une tâche associée à un jeu.

    create_round(game, task):
        Méthode abstraite pour créer un tour associé à un jeu et une tâche.

    create_user(username, order, game):
        Méthode abstraite pour créer un utilisateur associé à un jeu.

    create_user_vote(user, task, vote, round):
        Méthode abstraite pour créer un vote d'utilisateur associé à un jeu, une tâche et un tour.

    """

    @abstractmethod
    def create_task(self, game, name):
        pass

    @abstractmethod
    def create_round(self, game, task):
        pass

    @abstractmethod
    def create_user(self, username, order, game):
        pass

    @abstractmethod
    def create_user_vote(self, user, task, vote, round):
        pass


class ConcreteGameFactory(GameFactory):
    """
    Classe concrète représentant une fabrique de jeu.

    Cette classe hérite de GameFactory et implémente les méthodes abstraites pour créer des éléments liés au jeu.

    Attributes:
    -----------
    game : Game
        Instance de jeu associée à la fabrique.

    Methods
    --------
    create_task(game, name):
        Crée une tâche associée à un jeu.

    create_round(game, task):
        Crée un tour associé à un jeu et une tâche.

    create_user(username, order, game):
        Crée un utilisateur associé à un jeu.

    create_user_vote(user, task, vote, round):
        Crée un vote d'utilisateur associé à un jeu, une tâche et un tour.

    get_game():
        Obtient l'instance de jeu associée à la fabrique.

    update_current_game():
        Met à jour l'instance de jeu actuelle en supprimant les jeux supplémentaires.

    __str__():
        Obtient une représentation sous forme de chaîne de la fabrique.

    """

    def __init__(self, game):
        self.game = game

    def create_task(self, game, name):
        """
        Crée une tâche associée à un jeu.

        Parameters:
        -----------
        game : Game
            Jeu associé à la tâche.

        name : str
            Nom de la tâche.

        Returns:
        --------
        tuple
            Tuple contenant la tâche créée et un indicateur de création.

        """
        task, created = Task.objects.get_or_create(game=game, name=name)
        return (task, created)

    def create_round(self, game, task, finished=False):
        """
        Crée un tour associé à un jeu et une tâche.

        Parameters:
        -----------
        game : Game
            Jeu associé au tour.

        task : Task
            Tâche associée au tour.

        finished : bool, optional
            Indique si le tour est terminé (par défaut False).

        Returns:
        --------
        Round
            Instance de tour créée.

        """
        return Round.objects.create(game=game, task=task, finished=finished)

    def create_user(self, username, order, game):
        """
        Crée un utilisateur associé à un jeu.

        Parameters:
        -----------
        username : str
            Nom d'utilisateur.

        order : int
            Ordre de l'utilisateur.

        game : Game
            Jeu associé à l'utilisateur.

        Returns:
        --------
        tuple
            Tuple contenant l'utilisateur créé et un indicateur de création.

        """
        old_user = User.objects.filter(username=username).exists()
        if old_user:
            return (User.objects.get(username=username), False)
        user = User.objects.create(username=username, order=order, game=game)
        return (user, True)

    def create_user_vote(self, user, task, vote, round, voted=False):
        """
        Crée un vote d'utilisateur associé à un jeu, une tâche et un tour.

        Parameters:
        -----------
        user : User
            Utilisateur associé au vote.

        task : Task
            Tâche associée au vote.

        vote : int
            Valeur du vote.

        round : Round
            Tour associé au vote.

        voted : bool, optional
            Indique si l'utilisateur a voté (par défaut False).

        Returns:
        --------
        UserVote
            Instance de vote d'utilisateur créée.

        """
        return UserVote.objects.create(
            user=user, task=task, vote=vote, round=round, voted=voted
        )

    def get_game(self):
        """
        Obtient l'instance de jeu associée à la fabrique.

        Returns:
        --------
        Game or False
            Instance de jeu ou False si aucune instance n'est trouvée.

        """
        game = Game.objects.first()
        self.game = game
        if not game:
            return False
        return self.game

    @staticmethod
    def update_current_game():
        """
        Met à jour l'instance de jeu actuelle en supprimant les jeux supplémentaires.

        Returns:
        --------
        Game or None
            Instance de jeu mise à jour ou None si aucune instance n'est trouvée.

        """
        # s'il y a plus d'un jeu, obtenez le dernier et supprimez les autres
        if Game.objects.count() > 1:
            games = Game.objects.all()
            for game in games:
                if game != Game.objects.last():
                    game.delete()
        return Game.objects.first()

    def __str__(self):
        """
        Obtient une représentation sous forme de chaîne de la fabrique.

        Returns:
        --------
        str
            Représentation sous forme de chaîne de la fabrique.

        """
        return f"game - {self.game}"
