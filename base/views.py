from django.shortcuts import render, redirect, HttpResponse
from django.urls import reverse
import json
from django.core.cache import cache
from django.contrib.auth.decorators import login_required
from .models import User, Game, Task, UserVote, Round, Card
import os
from django.conf import settings
from django.utils import timezone
from .factory import ConcreteGameFactory
import datetime


factory = None  # Create a factory to create objects


def index(request):
    """
    Page d'Accueil

    Cette vue est la page principale de l'application. Elle affiche la tâche actuelle et l'utilisateur actuel,
    permettant à l'utilisateur de voter pour son ordre.

    Paramètres:
    -----------
        request : HttpRequest

                L'objet de requête envoyé par l'utilisateur.

    Returns:
    ---------
        HttpResponse

                L'objet de réponse HTTP qui sera rendu pour la page d'accueil.

    Rôle:
    -----
        Cette vue est utilisée pour afficher la page d'accueil de l'application. Elle affiche la tâche actuelle et l'utilisateur actuel, permettant à l'utilisateur de voter pour son ordre.
        Elle est également utilisée pour vérifier si la partie est terminée ou non, et si elle est terminée, elle redirige l'utilisateur vers la page de fin de partie.

    """
    global factory  # make the factory global to use it in the function

    factory = (
        ConcreteGameFactory.update_current_game()
    )  # restore the game from the database

    if not factory:
        # if the factory is not created, redirect the user to the init page to create a new game
        return redirect("init")

    game = factory  # get the current game in use

    if not game:
        return redirect("init")

    round = Round.objects.filter(game=game, finished=False).first()
    # check if there is a round that is not finished

    if not round:
        return redirect("game_end")

    task = round.task
    voters = round.votes.all()
    votes = UserVote.objects.filter(task=task)
    main_voter = None

    for vote in votes:
        if not vote.voted:
            # check if there is a user that didn't vote if so, use them as the main voter
            main_voter = vote.user
            break

    if not main_voter:
        # if all the users voted, play the next round
        round.finished = True
        round.save()
        return redirect("index")

    user_votes = UserVote.objects.filter(user=main_voter)

    context = {
        "game": game,
        "voters": voters,
        "tasks": game.tasks.all(),
        "main_task": task,
        "main_voter": main_voter,
        "votes": user_votes,
        "round": round,
    }

    return render(request, "index.html", context)


def init(request):
    """
    Initialiser le jeu

    Cette vue est la première vue de l'application où l'utilisateur peut choisir le mode de jeu.

    Parameters:
    -----------
        request : HttpRequest
            L'objet de requête envoyé par l'utilisateur.

    Returns:
    --------
        HttpResponse
            L'objet de réponse HTTP qui sera rendu pour la page d'initialisation.

    Rôle:
    -----
        Cette vue est utilisée pour afficher la première page de l'application, où l'utilisateur peut choisir le mode de jeu.
        Elle récupère les modes disponibles en français à l'aide de Google Traduction et les affiche à l'utilisateur.

    """
    modes = (
        Game.get_french_mode()
    )  # obtenir les modes en français avec Google Traduction
    return render(request, "init.html", {"modes": modes})


def start_game(request):
    """
    Démarrer le jeu

    Cette vue gère le mode de jeu et la création de la tâche de base avec la méthode POST.
    Elle enregistre le jeu dans la base de données et dans la fabrique.

    Parameters:
    -----------
        request : HttpRequest
            L'objet de requête envoyé par l'utilisateur.

    Returns:
    --------
        HttpResponse or HttpResponseRedirect
            L'objet de réponse HTTP qui sera rendu pour la page de démarrage du jeu ou une redirection vers la page d'ajout de tâches.

    Rôle:
    -----
        Cette vue est utilisée pour gérer le mode de jeu et la création de la tâche de base avec la méthode POST.
        Elle sauvegarde le jeu dans la base de données et dans la fabrique, puis redirige l'utilisateur vers la page d'ajout de tâches.

    """
    global factory

    if request.method == "POST":
        mode = int(
            request.POST.get("game-mode", 0)
        )  # 0 is the default value if the user doesn't choose a mode (it's impossible because the user can't submit the form without choosing a mode)
        base_task = request.POST.get("base-task", "")
        game = Game.get_instance(
            mode=mode, base_task=base_task
        )  # get the game instance with the singleton pattern
        factory = ConcreteGameFactory(game)  # create the factory with the game instance
        factory.update_current_game()  # update the game in the factory in case the user wants to reset the game

        return redirect("add_tasks")

    return render(request, "init.html")


def add_tasks(request):
    """
    Ajouter des tâches

    Cette vue gère la création de tâches avec la méthode POST et enregistre les tâches dans la base de données.

    Parameters:
    -----------
        request : HttpRequest
            L'objet de requête envoyé par l'utilisateur.

    Returns:
    --------
        HttpResponse
            L'objet de réponse HTTP qui sera rendu pour la page d'ajout de tâches.

    Rôle:
    -----
        Cette vue est utilisée pour gérer la création de tâches avec la méthode POST et enregistrer les tâches dans la base de données.
        Si l'utilisateur est redirigé depuis la vue add_users parce qu'il n'a ajouté aucune tâche, un message d'alerte est affiché.

    """
    global factory

    if not factory:
        return redirect("init")

    game = factory.get_game()

    if not game:
        return redirect("init")

    # Retourner l'utilisateur à la page d'initialisation si, d'une manière quelconque, le jeu n'est pas trouvé
    # (par exemple, si l'utilisateur supprime le jeu dans la base de données ou tente d'accéder à la page avec l'URL!!)
    if request.GET.get("returned", False):
        context = {
            "gotoadd": True,
            "game": game,
            "mustAdd": True,
            "alert": {
                "type": "danger",
                "message": "Vous devez ajouter au moins une tâche avant d'ajouter des utilisateurs !",
            },
        }
        return render(request, "add_tasks.html", context)

    if request.method == "POST":
        input_task = request.POST.get("task")
        task, created = factory.create_task(
            game=game, name=input_task
        )  # créer la tâche avec la fabrique
        if not created:
            context = {
                "gotoadd": True,
                "game": game,
                "tasks": game.tasks.all(),
                "alert": {
                    "type": "danger",
                    "message": "Cette tâche existe déjà !",
                },
            }
            return render(request, "add_tasks.html", context)

        game.tasks.add(
            task
        )  # ajouter la tâche à l'objet du jeu dans la base de données
        game.save()
        context = {
            "gotoadd": True,
            "game": game,
            "tasks": game.tasks.all(),
            "alert": {"type": "success", "message": "Tâche ajoutée avec succès !"},
        }
        return render(request, "add_tasks.html", context)

    return render(request, "add_tasks.html", {"game": game, "mustAdd": True})


def import_tasks(request):
    """
    Importer des tâches à partir d'un fichier JSON

    Si l'utilisateur dispose d'un fichier JSON contenant des tâches, il peut l'importer avec cette vue.
    *** Le format du JSON n'a pas d'importance, la seule chose qui compte est que le fichier JSON doit être un tableau d'objets avec un attribut de nom ***

    Parameters:
    -----------
        request : HttpRequest
            L'objet de requête envoyé par l'utilisateur.

    Returns:
    --------
        HttpResponse
            L'objet de réponse HTTP qui sera rendu pour la page d'importation de tâches.

    Rôle:
    -----
        Cette vue est utilisée pour importer des tâches à partir d'un fichier JSON. L'utilisateur peut choisir un fichier, et les tâches seront ajoutées au jeu en cours.
        Le format du JSON n'a pas d'importance, tant qu'il s'agit d'un tableau d'objets avec un attribut de nom.

    """
    global factory

    if not factory:
        return redirect("init")

    game = factory.get_game()

    if not game:
        return redirect("init")

    if request.method == "POST":
        file_path = request.FILES.get("json-path")  # obtenir le chemin du fichier
        if not file_path:
            context = {
                "gotoadd": False,
                "game": game,
                "mustAdd": True if len(game.tasks.all()) < 1 else False,
                "tasks": game.tasks.all(),
                "alert": {
                    "type": "danger",
                    "message": "Le fichier JSON n'a pas été trouvé !",
                },
            }
            return render(request, "add_tasks.html", context)

        file = file_path.read()  # lire le fichier
        tasks = json.loads(
            file
        )  # utiliser Json.loads pour convertir le fichier en objet python
        for task in tasks:
            task, created = factory.create_task(game=game, name=task["name"])
            game.tasks.add(task)
            game.save()

        context = {
            "gotoadd": False,  # forcer l'utilisateur à revenir à la vue add-tasks
            "game": game,
            "tasks": game.tasks.all(),
            "alert": {"type": "success", "message": "Tâches ajoutées avec succès !"},
        }
        return render(request, "add_tasks.html", context)

    return render(
        request,
        "add_tasks.html",
        {
            "game": game,
            "mustAdd": True,
        },
    )


def add_users(request):
    """
    Ajouter des utilisateurs

    Cette vue gère la création d'utilisateurs avec la méthode POST et enregistre les utilisateurs dans la base de données.

    Parameters:
    -----------
        request : HttpRequest
            L'objet de requête envoyé par l'utilisateur.

    Returns:
    --------
        HttpResponse
            L'objet de réponse HTTP qui sera rendu pour la page d'ajout d'utilisateurs.

    Rôle:
    -----
        Cette vue est utilisée pour gérer la création d'utilisateurs avec la méthode POST et enregistrer les utilisateurs dans la base de données.
        Si l'utilisateur est redirigé depuis la vue add_tasks parce qu'il n'a ajouté aucune tâche, un message d'alerte est affiché.

    """
    global factory

    if not factory:
        return redirect("init")

    game = factory.get_game()

    if not game:
        return redirect("init")

    if game.tasks.all().count() == 0:
        return redirect(
            reverse("add_tasks") + "?returned=true"
        )  # la même chose que la vue add_tasks

    if request.GET.get("returned", False):
        context = {
            "game": game,
            "mustAdd": True,
            "alert": {
                "type": "danger",
                "message": "Vous devez ajouter au moins un utilisateur avant de commencer la partie !",
            },
            "tasks": game.tasks.all(),
        }
        return render(request, "add_users.html", context)

    if request.method == "POST":
        user_name = request.POST.get("username")
        """
        Nous avons besoin d'un indice pour savoir quel utilisateur votera à chaque tour, nous ajoutons donc un attribut order au modèle utilisateur
        L'idée est de prendre le dernier utilisateur dans la base de données et d'incrémenter son order de 1
        """
        user = User.objects.last()
        order = (
            0  # valeur par défaut s'il n'y a pas d'utilisateur dans la base de données
        )
        if user:
            order = user.order + 1
        new_user, created = factory.create_user(
            username=user_name, order=order, game=game
        )
        if not created:
            context = {
                "game": game,
                "users": game.users.all(),
                "mustAdd": True if len(game.users.all()) < 2 else False,
                "tasks": game.tasks.all(),
                "alert": {
                    "type": "danger",
                    "message": "Cet utilisateur existe déjà !",
                },
            }
            return render(request, "add_users.html", context)

        game.users.add(new_user)
        game.save()

        context = {
            "game": game,
            "users": game.users.all(),
            "mustAdd": True if len(game.users.all()) < 2 else False,
            "tasks": game.tasks.all(),
            "alert": {"type": "success", "message": "Utilisateur ajouté avec succès !"},
        }
        return render(request, "add_users.html", context)

    context = {
        "game": game,
        "users": game.users.all(),
        "tasks": game.tasks.all(),
        "mustAdd": True,
    }
    return render(request, "add_users.html", context)


def init_rounds(request):
    """
    Initialiser les tours

    Cette vue est utilisée pour créer les tours et les votes pour chaque utilisateur à chaque tour.

    Parameters:
    -----------
        request : HttpRequest
            L'objet de requête envoyé par l'utilisateur.

    Returns:
    --------
        HttpResponseRedirect
            Redirection vers la page d'index après l'initialisation des tours.

    Rôle:
    -----
        Cette vue est utilisée pour créer les tours et les votes pour chaque utilisateur à chaque tour.
        Les tours sont créés pour chaque tâche du jeu, et les votes sont initialisés avec une valeur par défaut de 0.
        Ensuite, l'utilisateur est redirigé vers la page d'index.

    """
    global factory

    if not factory:
        return redirect("init")

    game = factory.get_game()

    if not game:
        return redirect("init")

    tasks = (
        game.tasks.all()
    )  # en Django, nous pouvons obtenir les sous-objets M2M comme ceci
    users = game.users.all()

    if tasks.count() == 0:
        return redirect(reverse("add_tasks") + "?returned=true")

    if users.count() == 0:
        return redirect(reverse("add_users") + "?returned=true")

    for task in tasks:
        round = factory.create_round(game=game, task=task)
        game.rounds.add(round)
        game.save()

        for user in users:
            vs = factory.create_user_vote(user=user, task=task, vote=0, round=round)
            round.votes.add(vs)
            round.save()

    return redirect("index")


def reset_game(request):
    """
    Réinitialiser le jeu

    En cas où l'utilisateur souhaite réinitialiser le jeu, cette vue supprimera tous les objets de la base de données,
    donnant à l'utilisateur la possibilité de commencer une nouvelle partie.

    Parameters:
    -----------
        request : HttpRequest
            L'objet de requête envoyé par l'utilisateur.

    Returns:
    --------
        HttpResponseRedirect
            Redirection vers la page d'initialisation après la réinitialisation du jeu.

    Rôle:
    -----
        Cette vue est utilisée pour réinitialiser le jeu. Elle supprime tous les utilisateurs, tâches et la partie de la base de données,
        donnant ainsi à l'utilisateur la possibilité de commencer une nouvelle partie.

    """
    global factory

    if not factory:
        return redirect("init")

    game = factory.get_game()

    if not game:
        return redirect("init")

    for user in game.users.all():
        user.delete()

    for task in game.tasks.all():
        task.delete()

    game.delete()

    return redirect("init")


def game_end(request):
    """
    Fin de partie

    Cette vue est utilisée pour mettre fin au jeu. Elle enregistrera la date de fin du jeu et créera un fichier JSON avec les votes finaux.

    Parameters:
    -----------
        request : HttpRequest
            L'objet de requête envoyé par l'utilisateur.

    Returns:
    --------
        HttpResponse
            L'objet de réponse HTTP qui sera rendu pour la page de fin de partie.

    Rôle:
    -----
        Cette vue est utilisée pour mettre fin au jeu. Elle enregistre la date de fin du jeu, crée un fichier JSON avec les votes finaux
        et renvoie les informations nécessaires pour afficher la page de fin de partie.

    """
    global factory

    if not factory:
        return redirect("init")

    game = factory.get_game()

    if not game:
        return redirect("init")

    if not game.end_at:
        game.end_at = timezone.now()
        game.save()

    local_dir = (
        settings.BASE_DIR
    )  # obtenir le répertoire de base du projet depuis settings.py
    file_name = "tasks.json"  # le nom de fichier que nous avons choisi
    file_path = os.path.join(
        local_dir, "static", "json", file_name
    )  # obtenir le chemin complet du fichier depuis le lecteur C
    final_votes = dict()
    final_votes["start_at"] = str(
        game.start_at
    )  # convertir la date en chaîne pour pouvoir la sauvegarder dans le fichier JSON
    final_votes["ended_at"] = str(game.end_at) if game.end_at else None
    final_votes["base_task"] = game.base_task
    final_votes["mode"] = game.get_current_mode_french()
    final_votes["tasks"] = list()

    for round in game.rounds.all():
        final_votes["tasks"].append(
            {
                "name": round.task.name,
                "final_vote": round.final_vote,
            }
        )

    with open(file_path, "w") as f:
        json.dump(final_votes, f)

    context = {
        "game": game,
        "json_file_path": file_path,
        "file_name": file_name,
    }

    return render(request, "game_end.html", context)


def new_vote(request, round_id, voter_id):
    """
    Nouveau vote

    Cette vue est utilisée pour enregistrer le vote de l'utilisateur et vérifier si tous les utilisateurs ont voté.
    Si c'est le cas, elle passera au tour suivant.

    Parameters:
    -----------
        request : HttpRequest
            L'objet de requête envoyé par l'utilisateur.
        round_id : int
            L'identifiant du tour en cours.
        voter_id : int
            L'identifiant de l'utilisateur qui vote.

    Returns:
    --------
        HttpResponseRedirect
            Redirection vers la page d'index ou la page de résultats du tour, selon la situation.

    Rôle:
    -----
        Cette vue est utilisée pour enregistrer le vote de l'utilisateur et vérifier si tous les utilisateurs ont voté.
        Si c'est le cas, elle marque le tour comme terminé et redirige soit vers la page d'index, soit vers la page de résultats du tour.

    """
    global factory

    if not factory:
        return redirect("init")

    game = factory.get_game()

    if not game:
        return redirect("init")

    round = Round.objects.filter(id=round_id).first()
    voter = User.objects.filter(id=voter_id).first()

    if request.method == "POST":
        selected_card = request.POST.get("selected-card")
        vote = UserVote.objects.filter(user=voter, task=round.task).first()
        vote.vote = Card.CARDS[selected_card]
        vote.voted = True
        vote.save()

        if (
            game.users.all().count() == round.votes.filter(voted=True).count()
        ):  # vérifier si tous les utilisateurs ont voté
            round.finished = True
            round.save()
            return redirect("round_results", round.id)

    return redirect("index")


def round_results(request, id):
    """
    Résultats du tour -- Vue logique principale

    Cette vue est utilisée pour calculer le vote final du tour en fonction du mode de jeu et vérifier si la partie est terminée ou non.

    Parameters:
    -----------
        request : HttpRequest
            L'objet de requête envoyé par l'utilisateur.
        id : int
            L'identifiant du tour en cours.

    Returns:
    --------
        HttpResponse
            L'objet de réponse HTTP qui sera rendu pour la page des résultats du tour.

    Rôle:
    -----
        Cette vue est utilisée pour calculer le vote final du tour en fonction du mode de jeu et vérifier si la partie est terminée ou non.
        Elle renvoie les informations nécessaires pour afficher la page des résultats du tour.

    """
    global factory

    if not factory:
        return redirect("init")

    game = factory.get_game()

    if not game:
        return redirect("init")

    round = Round.objects.get(id=id)

    if not round.finished:
        return redirect("index")

    if game.mode == 1:  # Strict
        votes = round.votes.all()
        vote_result = list()

        for vote in votes:
            vote_result.append(vote.vote)

        if len(set(vote_result)) == 1 and vote_result[0] != -2 and vote_result[0] != -1:
            round.finished = True
            round.final_vote = vote_result[0]
            round.save()

            context = {
                "game": game,
                "round": round,
                "restart_round": False,
                "last_round": True
                if round.task == game.tasks.last()
                else False,  # indicate if it's the last round or not
                "result": round.final_vote,
                "alert": {
                    "type": "success",
                    "message": "Les votes sont égaux, la partie est terminée !",
                },
            }

            return render(request, "round_results.html", context)
        else:
            round.finished = False
            round.save()

            for vote in votes:
                vote.voted = False
                vote.save()

            context = {
                "game": game,
                "round": round,
                "restart_round": True,
                "alert": {
                    "type": "danger",
                    "message": "Les votes ne sont pas égaux, recommencez !",
                },
            }

            if (
                -1 in vote_result
            ):  # s'il y a un vote café, nous montrerons un message à l'utilisateur pour lui dire que quelqu'un a besoin d'un café
                # Pour les lignes suivantes, une logique pour montrer un message à l'utilisateur pour lui dire que quelqu'un a besoin d'un café avec une personne ou plusieurs personnes
                users_votes = UserVote.objects.filter(round=round, vote=-1).all()
                message = ""
                cpt = 0

                for i, vote in enumerate(users_votes):
                    message += f"'{vote.user.username}'"
                    if i < len(users_votes) - 1:
                        message += " et "
                    cpt += 1

                if cpt == 1:
                    context["need_cafe"] = f"{message} a besoin d'un café !"
                else:
                    context["need_cafe"] = f"{message} ont besoin d'un café !"

            return render(request, "round_results.html", context)

    elif game.mode == 2:  # Average
        votes = round.votes.all()
        vote_result = [
            vote.vote for vote in votes if vote.vote != -1 and vote.vote != -2
        ]  # remove the coffee and interrogation votes

        if len(vote_result) == 0:  # all -1 or all -2:
            round.finished = False
            round.save()

            for vote in votes:
                vote.voted = False
                vote.save()

            if (
                votes[0].vote == -1
            ):  # obtenir le premier vote pour vérifier s'il s'agit d'un café, si c'est le cas, tous les utilisateurs ont besoin d'un café
                context = {
                    "game": game,
                    "round": round,
                    "restart_round": True,
                    "alert": {
                        "type": "danger",
                        "message": "Les joueurs ont besoin d'un café !",
                    },
                }
                return render(request, "round_results.html", context)
            else:  # s'il ne s'agit pas d'un café, tous les utilisateurs ont besoin d'une interrogation
                # todo gérer la situation de l'interrogatoire
                context = {
                    "game": game,
                    "round": round,
                    "restart_round": True,
                    "alert": {
                        "type": "danger",
                        "message": "Les joueurs ont besoin d'une interrogation !",
                    },
                }
                return render(request, "round_results.html", context)

        # if the votes are not all coffees or all interrogations, we will calculate the average
        round.final_vote = int(sum(vote_result) / len(vote_result))
        round.set_average()
        round.finished = True
        round.save()

        context = {
            "game": game,
            "round": round,
            "restart_round": False,
            "last_round": True if round.task == game.tasks.last() else False,
            "result": round.final_vote,
            "alert": {
                "type": "success",
                "message": "La partie est terminée !",
            },
        }

        return render(request, "round_results.html", context)

    context = {
        "game": game,
        "round": round,
    }

    return render(request, "round_results.html", context)


def pause_game(request):
    """
    Mettre en pause la partie

    Cette vue est utilisée pour mettre en pause la partie. Elle sauvegardera la date actuelle dans un fichier JSON et supprimera l'instance de jeu de la base de données.

    Parameters:
    -----------
        request : HttpRequest
            L'objet de requête envoyé par l'utilisateur.

    Returns:
    --------
        HttpResponse
            L'objet de réponse HTTP qui sera rendu pour la page de jeux en pause.

    Rôle:
    -----
        Cette vue est utilisée pour mettre en pause la partie. Elle sauvegardera la date actuelle dans un fichier JSON, enregistre les détails actuels de la partie, puis supprimera l'instance de jeu de la base de données.

    """
    global factory

    if not factory:
        return redirect("init")

    game = factory.get_game()

    if not game:
        return redirect("init")

    local_dir = (
        settings.BASE_DIR
    )  # obtenir le répertoire de base du projet à partir de settings.py
    current_date = (
        str(datetime.datetime.now())[:16].replace(" ", "--").replace(":", "-")
    )  # obtenir la date actuelle pour l'utiliser comme nom de fichier et remplacer les espaces et les deux points par des tirets ensuite chercher uniquement les 16 premiers caractères donc l'année, le mois, le jour, l'heure et les minutes

    file_name = f"pause-{current_date}.json"  # le nom de fichier que nous avons choisi avec la date actuelle
    file_path = os.path.join(
        local_dir, "static", "json", "pauses", file_name
    )  # obtenir le chemin complet du fichier depuis le lecteur C via le répertoire static/json/pauses
    pause_data = dict()
    pause_data["start_at"] = (
        str(game.start_at) if game.start_at else None
    )  # convertir la date en chaîne pour pouvoir la sauvegarder dans le fichier JSON
    pause_data["mode"] = game.mode  # sauvegarder le mode de jeu
    pause_data["base_task"] = game.base_task  # sauvegarder la tâche de base
    (
        pause_data["users"],
        pause_data["tasks"],
        pause_data["rounds"],
        pause_data["UserVote"],
    ) = (
        list(),
        list(),
        list(),
        list(),
    )  # manières simples de créer des listes vides en python

    for user in game.users.all():  # sauvegarder les utilisateurs
        pause_data["users"].append({"username": user.username, "order": user.order})

    for task in game.tasks.all():  # sauvegarder les tâches
        pause_data["tasks"].append({"name": task.name})

    for round in game.rounds.all():  # sauvegarder les tours
        pause_data["rounds"].append(
            {"task": round.task.name, "finished": round.finished}
        )

    for vote in UserVote.objects.all():  # sauvegarder les votes
        pause_data["UserVote"].append(
            {
                "user": vote.user.username,
                "task": vote.task.name,
                "vote": vote.vote,
                "voted": vote.voted,
                "round": vote.round.id,
            }
        )

    with open(file_path, "w") as f:
        json.dump(pause_data, f)  # sauvegarder les données dans le fichier JSON

    game.delete()  # supprimer l'instance de jeu de la base de données pour éviter les conflits avec les autres parties

    return redirect("games_in_pause")


def games_in_pause(request):
    """
    Jeux en pause

    Cette vue est utilisée pour montrer à l'utilisateur que la partie est en pause.

    Parameters:
    -----------
        request : HttpRequest
            L'objet de requête envoyé par l'utilisateur.

    Returns:
    --------
        HttpResponse
            L'objet de réponse HTTP qui sera rendu pour la page des jeux en pause.

    Rôle:
    -----
        Cette vue est utilisée pour informer l'utilisateur que la partie est en pause. Elle récupère la liste des noms de fichiers de pause dans le répertoire correspondant et les transmet au modèle pour affichage.

    """
    local_dir = settings.BASE_DIR
    file_path = os.path.join(local_dir, "static", "json", "pauses")
    names = (
        list()
    )  # le but de cette liste est de stocker les noms des fichiers JSON dans le répertoire static/json/pauses
    for file in os.listdir(file_path):
        if file.endswith(".json"):
            names.append(
                file.replace("pause-", "").replace(".json", "")
            )  # remplacer le nom du fichier par la date du jeu

    return render(
        request, "game_in_pause.html", {"names": names}
    )  # transmettre les noms des fichiers au template pour les affichage


def restart_game_in_pause(request):
    """
    Redémarrer le jeu en pause

    Cette vue est utilisée pour redémarrer le jeu en pause à partir du fichier JSON de pause correspondant.

    Parameters:
    -----------
        request : HttpRequest
            L'objet de requête envoyé par l'utilisateur.

    Returns:
    --------
        HttpResponse
            L'objet de réponse HTTP qui redirige l'utilisateur vers la page d'accueil.

    Rôle:
    -----
        Cette vue prend en charge la requête POST pour redémarrer le jeu en pause. Elle récupère le fichier JSON de pause, crée une nouvelle instance de jeu avec les informations du fichier, et redirige l'utilisateur vers la page d'accueil.

    """
    if request.method == "POST":
        game_date = request.POST.get("game-date")
        local_dir = settings.BASE_DIR
        file_path = os.path.join(local_dir, "static", "json", "pauses")
        file_name = f"pause-{game_date.replace(':', '-')}.json"
        file_path = os.path.join(file_path, file_name)
        try:  # essayer d'ouvrir le fichier JSON au cas où il y aurait une erreur on veut pas que l'application plante
            with open(file_path, "r") as f:
                pause = json.load(f)
        except:
            names = list()
            for file in os.listdir(file_path):
                if file.endswith(".json"):
                    names.append(file.replace("pause-", "").replace(".json", ""))
            return render(
                request,
                "game_in_pause.html",
                {
                    "names": names,
                    "alert": {
                        "type": "danger",
                        "message": "Une erreur s'est produite lors de la lecture du fichier !",
                    },
                },
            )
        game = Game.get_instance(mode=pause["mode"], base_task=pause["base_task"])
        factory = ConcreteGameFactory(
            game
        )  # on part du principe que le jeu est déjà créé dans la base de données mais pas dans la fabrique
        factory.update_current_game()  # mettre à jour le jeu dans la fabrique
        for user in pause["users"]:
            user, created = factory.create_user(
                username=user["username"], order=user["order"], game=game
            )  # en django, on peut chercher un objet et un varible qui indique si l'objet a été créé ou non, le cas où l'utilisateur existe déjà dans la base de données on fait rien
            game.users.add(
                user
            )  # ajouter l'utilisateur à l'objet du jeu dans la base de données
        for task in pause["tasks"]:
            task, created = factory.create_task(name=task["name"], game=game)
            game.tasks.add(task)
        for round in pause["rounds"]:
            round = factory.create_round(
                task=Task.objects.get(name=round["task"]),
                game=game,
                finished=round["finished"],
            )
            game.rounds.add(round)
        for vote in pause["UserVote"]:
            round = None
            round = Round.objects.filter(
                id=vote["round"]
            ).first()  # on cherche le tour dans la base de données et on prend le premier objet
            if not round:
                round = Round.objects.filter(
                    task=Task.objects.get(name=vote["task"])
                ).first()
            uv = factory.create_user_vote(
                user=User.objects.filter(username=vote["user"]).first(),
                task=Task.objects.filter(name=vote["task"]).first(),
                vote=vote["vote"],
                voted=vote["voted"],
                round=round,
            )
            round.votes.add(uv)
        game.start_at = (
            pause["start_at"] if pause["start_at"] else None
        )  # remettre la date de début du jeu
        game.save()

        return redirect("index")
    return redirect("games_in_pause")


def delete_game_in_pause(request):
    """
    Supprimer le jeu en pause

    Cette vue est utilisée pour supprimer le jeu en pause en fonction de la date du jeu.

    Parameters:
    -----------
        request : HttpRequest
            L'objet de requête envoyé par l'utilisateur.

    Returns:
    --------
        HttpResponse
            L'objet de réponse HTTP redirige l'utilisateur vers la page des jeux en pause.

    Rôle:
    -----
        Cette vue prend en charge la requête POST pour supprimer le jeu en pause en fonction de la date du jeu. Elle supprime le fichier correspondant et renvoie l'utilisateur vers la page des jeux en pause avec un message d'alerte approprié.

    """
    if request.method == "POST":
        game_date = request.POST.get("game-date")
        local_dir = settings.BASE_DIR
        file_path = os.path.join(local_dir, "static", "json", "pauses")
        file_name = f"pause-{game_date.replace(':', '-')}.json"
        file_path_with_file = os.path.join(file_path, file_name)

        try:
            os.remove(
                file_path_with_file
            )  # essayer de supprimer le fichier JSON si une erreur se produit on veut pas que l'application plante
        except:
            names = list()
            for file in os.listdir(file_path):
                if file.endswith(".json"):
                    names.append(file.replace("pause-", "").replace(".json", ""))
            return render(
                request,
                "game_in_pause.html",
                {
                    "names": names,
                    "alert": {
                        "type": "danger",
                        "message": "Une erreur s'est produite lors de la suppression du fichier !",
                    },
                },
            )
        names = list()
        for file in os.listdir(file_path):
            if file.endswith(".json"):
                names.append(file.replace("pause-", "").replace(".json", ""))
        return render(
            request,
            "game_in_pause.html",
            {
                "names": names,
                "alert": {
                    "type": "success",
                    "message": "La partie a été supprimée avec succès !",
                },
            },
        )

    return redirect("games_in_pause")
