from django.db import models
from deep_translator import GoogleTranslator
from django.utils import timezone


from django.db import models
from deep_translator import GoogleTranslator
from django.utils import timezone


class User(models.Model):
    """
    Classe représentant un utilisateur dans l'application.

    Cette classe permet de créer un utilisateur (ou joueur) et de l'ajouter à une partie.
    L'utilisateur peut voter pour une tâche.

    Paramètres :
    ------------
        username : str

            Nom de l'utilisateur.

        order : int

            Ordre de passage de l'utilisateur dans la partie.

        game : Game

            Partie dans laquelle l'utilisateur est présent.

    Attributs :
    -----------
        username : str

            Nom de l'utilisateur.

        order : int

            Ordre de passage de l'utilisateur dans la partie.

        game : Game

            Partie dans laquelle l'utilisateur est présent.

    Methods
    --------
        __str__():
            Méthode spéciale pour obtenir une représentation sous forme de chaîne de l'objet.


    Subclasses
    ----------
        Meta:
            Classe interne pour définir les options du modèle. comme l'ordre de tri par défaut.
    """
    username = models.CharField(max_length=150, unique=True)
    order = models.IntegerField(default=0)
    game = models.ForeignKey("Game", on_delete=models.CASCADE)

    def __str__(self):
        """
        Obtient une représentation sous forme de chaîne de l'utilisateur.

        Returns:
        --------
        str
            Représentation sous forme de chaîne de l'utilisateur.

        """
        return f"{self.username}"

    class Meta:
        ordering = ["order"]


class SingletonMixin:
    """
    Mixin utilisé pour implémenter le modèle singleton.

    Cette mixin permet d'assurer qu'il n'y a qu'une seule instance d'une classe.

    Methods
    --------
    get_instance( * * kwargs):
        Obtient l'instance unique de la classe ou la crée si elle n'existe pas encore.

    """

    """
    Mixin utilisé pour implémenter le modèle singleton..

    Cette mixin permet d'assurer qu'il n'y a qu'une seule instance d'une classe.

    Methods
    --------
        get_instance( * * kwargs):
            Obtient l'instance unique de la classe ou la crée si elle n'existe pas encore.

    """

    _instance = None

    @classmethod
    def get_instance(cls, **kwargs):
        """
        Obtient l'instance unique de la classe ou la crée si elle n'existe pas encore.

        Parameters:
        -----------
        * * kwargs :
            Arguments pour la création de l'instance.

        Returns:
        --------
        SingletonMixin
            Instance unique de la classe.

        """
        if not cls._instance:
            cls._instance = cls.objects.create(**kwargs)
        else:
            cls._instance.update(**kwargs)
        return cls._instance


class Game(models.Model, SingletonMixin):
    """
    Classe représentant une partie dans l'application.

    Cette classe permet de créer et gérer une partie, avec des utilisateurs, des tâches et des rounds.

    Paramètres :
    ------------
        user : base.User

            Utilisateur associé à la partie.

        task : base.Task

            Tâche associée à la partie.

        mode : int

            Mode de la partie (1 : Strict, 2 : Average).

        round : base.Round

            Round associé à la partie.

        base_task : str

            Tâche de base de la partie.

        start_at : datetime.datetime

            Date et heure de début de la partie.

        end_at : datetime.datetime

            Date et heure de fin de la partie. (None si la partie n'est pas terminée donc None par défaut)

    Attributs :
    -----------
        MODES : tuple

            Modes disponibles pour une partie.

        users : ManyToManyField

            Utilisateurs associés à la partie.

        tasks : ManyToManyField

            Tâches associées à la partie.

        mode : int

            Mode de la partie.

        rounds : ManyToManyField

            Rounds associés à la partie.

        base_task : str

            Tâche de base de la partie.

        start_at : DateTimeField

            Date et heure de début de la partie.

        end_at : DateTimeField

            Date et heure de fin de la partie.

    Methods
    --------
        __str__():
            Méthode spéciale pour obtenir une représentation sous forme de chaîne de l'objet.

        get_french_mode(): -> Staticmethod
            Obtient la traduction en français des modes de la partie.

        get_current_mode_french():
            Obtient la traduction en français du mode actuel de la partie.

        get_game():
            Obtient l'instance unique de la partie.

        destroy():
            Détruit la partie en supprimant les utilisateurs, tâches et rounds associés.

        update(** kwargs):
            Met à jour les paramètres de la partie.

        create_game(mode, ** kwargs): -> Staticmethod
            Crée une nouvelle instance de la partie en utilisant le modèle singleton.

        add_task(task):
            Ajoute une tâche à la partie.

        add_tasks(tasks):
            Ajoute plusieurs tâches à la partie.

        add_user(user):
            Ajoute un utilisateur à la partie.

        add_users(users):
            Ajoute plusieurs utilisateurs à la partie.

        add_round(round):
            Ajoute un round à la partie.

        end_game():
            Termine la partie.

    """

    MODES = (
        (1, "Strict"),
        (2, "Average"),
    )

    users = models.ManyToManyField("User", blank=True, related_name="game_users")
    tasks = models.ManyToManyField("Task", blank=True, related_name="game_tasks")
    mode = models.IntegerField(default=1, choices=MODES)
    rounds = models.ManyToManyField("Round", blank=True, related_name="game_rounds")
    base_task = models.CharField(max_length=50, blank=True, null=True)
    start_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    end_at = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        """
        Obtient une représentation sous forme de chaîne de la partie.

        Returns:
        --------
        str
            Représentation sous forme de chaîne de la partie.

        """
        return self.MODES[self.mode - 1][1]

    @staticmethod
    def get_french_mode():
        """
        Obtient la traduction en français des modes de la partie.

        Returns:
        --------
        dict
            Traduction en français des modes de la partie.

        """
        res = dict()
        for k, v in Game.MODES:
            res[k] = GoogleTranslator(source="en", target="fr").translate(v)
        return res

    def get_current_mode_french(self):
        """
        Obtient la traduction en français du mode actuel de la partie.

        Returns:
        --------
        str
            Traduction en français du mode actuel de la partie.

        """
        return GoogleTranslator(source="en", target="fr").translate(
            self.MODES[self.mode - 1][1]
        )

    def get_game(self):
        """
        Obtient l'instance unique de la partie.

        Returns:
        --------
        Game
            Instance unique de la partie.

        """
        game = Game.objects.first()
        if not game:
            return False
        return game

    def destroy(self):
        """
        Détruit la partie en supprimant les utilisateurs, tâches et rounds associés.

        """
        lis = [self.users, self.tasks, self.rounds]
        for l in lis:
            for i in l.all():
                i.delete()
        self.base_task = None
        self.start_at = timezone.now()
        self.end_at = None
        self.save()

    def update(self, **kwargs):
        """
        Met à jour les paramètres de la partie.

        Parameters:
        -----------
        kwargs :
            Nouveaux paramètres.

        """
        for ins in [self.users, self.tasks, self.rounds]:
            for i in ins.all():
                i.delete()
        self.base_task = None
        self.start_at = timezone.now()
        self.end_at = None
        self.mode = kwargs["mode"]

        for k, v in kwargs.items():
            setattr(self, k, v)
        self.save()

    @staticmethod
    def create_game(mode, **kwargs):
        """
        Crée une nouvelle instance de la partie en utilisant le modèle singleton.

        Parameters:
        -----------
        mode : int
            Mode de la partie.

        * * kwargs :
            Autres paramètres.

        Returns:
        --------
        Game
            Nouvelle instance de la partie.

        Raises:
        --------
        ValueError
            Si le mode n'est pas valide.

        """
        if mode in [1, 2]:
            return Game.get_instance(**kwargs)
        else:
            raise ValueError("Invalid mode")

    def add_task(self, task):
        """
        Ajoute une tâche à la partie.

        Parameters:
        -----------
        task : Task
            Tâche à ajouter.

        """
        self.tasks.add(task)
        self.save()

    def add_tasks(self, tasks):
        """
        Ajoute plusieurs tâches à la partie.

        Parameters:
        -----------
        tasks : list
            Liste de tâches à ajouter.

        """
        for t in tasks:
            self.tasks.add(t)
        self.save()

    def add_user(self, user):
        """
        Ajoute un utilisateur à la partie.

        Parameters:
        -----------
        user : User
            Utilisateur à ajouter.

        """
        self.users.add(user)
        self.save()

    def add_users(self, users):
        """
        Ajoute plusieurs utilisateurs à la partie.

        Parameters:
        -----------
        users : list
            Liste d'utilisateurs à ajouter.

        """
        for u in users:
            self.users.add(u)
        self.save()

    def add_round(self, round):
        """
        Ajoute un round à la partie.

        Parameters:
        -----------
        round : Round
            Round à ajouter.

        """
        self.rounds.add(round)
        self.save()

    def end_game(self):
        """
        Termine la partie.

        """
        self.end_at = timezone.now()
        self.save()


class Task(models.Model):
    """
    Classe représentant une tâche dans l'application.

    Cette classe permet de créer une tâche associée à une partie.

    Paramètres :
    ------------
        name : str

            Nom de la tâche.

        game : Game

                Partie associée à la tâche.

    Attributs :
    -----------
        name : str

            Nom de la tâche.

        game : Game

                Partie associée à la tâche.

    Methods
    --------
        __str__():
            Méthode spéciale pour obtenir une représentation sous forme de chaîne de l'objet.

    """

    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    name = models.CharField(max_length=1024, unique=True)

    def __str__(self):


        """
        Obtient une représentation sous forme de chaîne de la tâche.

        Returns:
        --------
        str
            Représentation sous forme de chaîne de la tâche.

        """
        return self.name

class Round(models.Model):
    """
    Classe représentant un tour dans l'application.

    Cette classe permet de créer un tour associé à une partie et une tâche.

    Paramètres :
    ------------
        game : Game
            Partie associée au tour.

        task : Task
            Tâche associée au tour.

    Attributs :
    -----------
        game : Game
            Partie associée au tour.

        task : Task
            Tâche associée au tour.

        votes : ManyToManyField
            Collection des votes associés à ce tour.

        finished : bool
            Indique si le tour est terminé.

        final_vote : int
            Vote final pour le tour.

    Methods
    --------
        __str__():
            Méthode spéciale pour obtenir une représentation sous forme de chaîne de l'objet.

        set_average():
            Calcule et définit la moyenne des votes du tour.

        add_vote(vote):
            Ajoute un vote au tour.

    """

    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    votes = models.ManyToManyField("UserVote", blank=True, related_name="round_votes")
    finished = models.BooleanField(default=False)
    final_vote = models.IntegerField(default=0)

    def __str__(self):
        """
        Obtient une représentation sous forme de chaîne du tour.

        Returns:
        --------
        str
            Représentation sous forme de chaîne du tour.

        """
        return f"{self.task.name} - round"

    def set_average(self):
        """
        Calcule la moyenne des votes du tour et la définit comme vote final.

        """
        cards = Card.CARDS
        if self.final_vote in cards.values():
            self.final_vote
            self.save()
        else:
            diff = 100
            closest = 0
            for k, v in cards.items():
                if abs(v - self.final_vote) < diff:
                    diff = abs(v - self.final_vote)
                    closest = v
            self.final_vote = closest
            self.save()

    def add_vote(self, vote):
        """
        Ajoute un vote au tour.

        Parameters:
        -----------
        vote : UserVote
            Vote à ajouter.

        """
        self.votes.add(vote)
        self.save()


class Card(models.Model):
    """
    Classe représentant une carte de vote dans l'application.

    Cette classe définit les cartes de vote possibles avec leurs valeurs associées.

    Attributes:
    -----------
        CARDS : dict
            Dictionnaire contenant les cartes de vote et leurs valeurs associées.

    """

    CARDS = {
        "cartes_0": 0,
        "cartes_1": 1,
        "cartes_2": 2,
        "cartes_3": 3,
        "cartes_5": 5,
        "cartes_8": 8,
        "cartes_13": 13,
        "cartes_20": 20,
        "cartes_40": 40,
        "cartes_100": 100,
        "cartes_cafe": -1,
        "cartes_interro": -2,
    }
    pass


class UserVote(models.Model):
    """
    Classe représentant le vote d'un utilisateur pour une tâche donnée dans une partie.

    Paramètres :
    ------------
        user : User
            Utilisateur associé au vote.

        task : Task
            Tâche associée au vote.

        vote : int
            Valeur du vote.

        voted : bool
            Indique si l'utilisateur a voté.

        round : Round
            Tour associé au vote.

    Methods
    --------
        __str__():
            Méthode spéciale pour obtenir une représentation sous forme de chaîne de l'objet.

        get_card():
            Obtient la carte associée à la valeur du vote.

    """

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    vote = models.IntegerField(default=0)
    voted = models.BooleanField(default=False)
    round = models.ForeignKey(Round, on_delete=models.CASCADE)

    def __str__(self):
        """
        Obtient une représentation sous forme de chaîne du vote de l'utilisateur.

        Returns:
        --------
        str
            Représentation sous forme de chaîne du vote de l'utilisateur.

        """
        return f"{self.user.username} - {self.round}"

    def get_card(self):
        """
        Obtient la carte associée à la valeur du vote.

        Returns:
        --------
        str
            Nom de la carte associée à la valeur du vote.

        """
        for k, v in Card.CARDS.items():
            if v == self.vote:
                return k
