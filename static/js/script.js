function submitNewUserForm(btn) {
    const input = document.getElementById(btn.dataset.input);
    const form = document.getElementById(btn.dataset.form);
    const errer = document.getElementById('new-user-alert');

    if (input.value.length > 3) {
        errer.classList.add('d-none');
        btn.disabled = true;
        btn.innerHTML = '<i class="fa fa-spinner fa-spin"></i>';
        form.submit();
    } else {
        errer.classList.remove('d-none');
    }
}


const moreOptionToggleBtns = document.querySelectorAll('.moreOptionToggle');
moreOptionToggleBtns.forEach(btn => {
    if (!btn || !btn.dataset.menu) return
    const menu = document.getElementById(btn.dataset.menu);
    if (!menu) return;
    btn.addEventListener('click', () => {
        menu.classList.toggle('active');
        if (menu.classList.contains('active')) {
            document.addEventListener('click', closeMenuOnClickOutside(btn, menu));
        }
    })
})



function closeMenuOnClickOutside(btn, menu) {
    return function (e) {
        if (e.target != btn && e.target != menu && !menu.contains(e.target)) {
            menu.classList.remove('active');
            document.removeEventListener('click', closeMenuOnClickOutside);
        }
    }
}




function handleNewItemSubmit(btn) {
    const form = document.querySelector('#new-item');
    const input = form.querySelector('#item');
    const errerAlert = document.querySelector('#new-item-alert');
    const inputVal = input.value;
    if (inputVal.length > 3) {
        errerAlert.classList.add('d-none');
        btn.disabled = true;
        btn.innerHTML = '<i class="fa fa-spinner fa-spin"></i>';
        form.submit();
    } else {
        errerAlert.classList.remove('d-none');
    }

}

function handleAddTaskSection(element) {
    const sectionName = element.getAttribute('data-sectionGole');
    const section = document.querySelector(`form[data-sectionName="${sectionName}"]`);
    const menuItems = document.querySelectorAll('.add-tasks-menu-item');
    menuItems.forEach(item => item.classList.remove('active'));
    element.classList.add('active');
    document.querySelectorAll('form').forEach(form => form.classList.add('d-none'));
    section.classList.remove('d-none');
}


function handleDisplkayFileName() {
    var fullPath = document.getElementById('json-path').value;
    const nameHolder = document.getElementById('fileNameHolder');
    if (fullPath) {
        var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));

        var filename = fullPath.substring(startIndex);
        var currentData = document.getElementById('temp-data');
        var currentDataMenu = currentData.querySelector('#temp-data-menu');
        var importTaskBtn = document.querySelector('#importTaskBtn');

        if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
            filename = filename.substring(1);
        }

        if (filename.length > 20) {
            filename = filename.substring(0, 20) + '...';
        }
        filename = 'Fichier : ' + filename;
        nameHolder.innerHTML = filename;
        var file = document.getElementById('json-path').files[0];
        var reader = new FileReader();
        currentData.classList.remove('d-none');
        reader.onload = function (e) {
            var json = JSON.parse(e.target.result);
            json.forEach(element => {
                var li = document.createElement('li');
                li.innerHTML = element.name;
                currentDataMenu.appendChild(li);
            });
        };
        importTaskBtn.innerHTML = 'Importer';
        reader.readAsText(file);



    }
}