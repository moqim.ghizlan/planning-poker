asgiref==3.7.2
beautifulsoup4==4.12.2
certifi==2023.11.17
charset-normalizer==3.3.2
deep-translator==1.11.4
Django==4.2.7
django-bootstrap-v5==1.0.11
fontawesomefree==6.5.0
idna==3.6
requests==2.31.0
soupsieve==2.5
sqlparse==0.4.4
tzdata==2023.3
urllib3==2.1.0
docutils==0.20.1