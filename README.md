# Planning Poker

![Image text](https://gitlab.com/moqim.ghizlan/planning-poker/-/raw/main/static/images/icon.png?ref_type=heads)

---

## Bienvenue sur le README de notre jeu.

### Explication du projet

Ce projet est un travaille de groupe en sens d'un model En Master 1 en Informatque '.

### Note très importante

Il ne faut surtout pas le lancer sur [GITPOD](https://www.gitpod.io/) car le projet returnera des erreur de type { csrf } et nous ne savons pas pourquoi.

### Les membres de groupe

Ce travail a été fait donc par :

- GHIZLAN Moqim [@moqim.ghizlan](https://gitlab.com/moqim.ghizlan)
- JOMAA Akrem [@akrem_jomaa](https://gitlab.com/akrem_jomaa.ghizlan)
- DOUAKHA Mohamed Islam [@islemdouakha](https://gitlab.com/islemdouakha)

### Lancement du projet

- Tout d'abord il vous faut cloner le projet dans le répertoire de votre choix avec la commande:

```
$ git clone https://gitlab.com/moqim.ghizlan/planning-poker.git
```

- Ensuite vous allez se mettre dans le dossier du projet:

```
$ cd planning-poker/
```

- Puis installer les requirements pour ce projet, faites :

```
$ pip install -r requirements.txt
```

- En suite, il vous faut créer les migrations:

```
$ python manage.py makemigrations
```

- En suite, il vous faut migrer la base des données:

```
$ python manage.py migrate
```

- En suite, pour voir les documentations, vous douvez créer un compte d'admin:

* Et vous entrez vos informations:

```
$ python manage.py createsuperuser
```

- Au final, pour lancer le projet, vous faites:

```
$ python manage.py runserver
```

### Tests

- Pour lancer les tests, vous faites :

```
$ python manage.py test base
```

### Documentations

- Pour voir documentations, RDV sur le site :

```
$ http://localhost:8000/admin/doc/
```

- Dans ce site, vous allez voir les docs de les modèles et les controleurs.
